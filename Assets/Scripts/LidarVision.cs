﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LidarVision : MonoBehaviour {

	// Distance from robot to obstacle.
	public float distanceFromObstacle = 0f;

	/*
	 * LIDAR
	*/
	// Distance of laser from robot lens
	public float maxDistance;

	// Used to draw point cloud on geospatial data.
	public GameObject pointCloud;

	// Used to store the generated point cloud
	private Transform pointCloudGenerator;

	// Coordinate of collided object from WorldSpace and will be used by Robot for navigating.
	private Vector3 worldSpaceView;

	// Is used for checking any collision between the ray and the obstacle.
	public bool hasDetected;

	public bool hasDetectGoal = false;

	// Use this for initialization
	void Start () {	
		worldSpaceView = new Vector3 (0f,0f,0f);
		pointCloudGenerator = GameObject.Find ("Point Cloud Generator").transform;
		hasDetected = false;
	}
	
	// Is used to build map for localization.
	void Update () {
		if (!hasDetectGoal) {

			Ray ray = new Ray (transform.position, transform.up);
			RaycastHit hit;

			// Draw laser from lidar to Game Object.
			Debug.DrawLine (transform.position, transform.position + transform.up * maxDistance, Color.red);

			// Checks the collision between obstacle and the laser then gets the current distance from ray origin to collided object.
			if (Physics.Raycast (ray, out hit, maxDistance, 1 << 10)) {			
				hasDetected = true;

				// Get the current distance from robot to obstacle
				distanceFromObstacle = hit.distance;

				worldSpaceView = hit.point;

				pointCloud = Instantiate (pointCloud);
				pointCloud.transform.position = worldSpaceView;
				pointCloud.transform.parent = pointCloudGenerator;
			} else
				distanceFromObstacle = hit.distance;
		} 
	}
}
