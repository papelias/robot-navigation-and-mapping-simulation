﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour {

	public RenderTexture rt;

	// Used to get the distance from robot to obstacle 
	private LidarVision vision;

	// Used to get the angle of lidar rotation.
	private Lidar lidar;

	// Speed of translating from start point to end point.
	public float speed = 20f;

	// Speed of rotating the robot
	public float rotationSpeed = 1f;

	// maximum distance that the laser can travel.
	private float maxDistance = 0f;

	// selected point cloud
	private int index;

	// The distance of each point cloud.
	private float[] pointCloudDistance;

	// Directions of the Lidar in one iteration.
	private float[] lidarDirections;

	// Current number of point cloud being collected.
	private int count;

	// maximum number of point cloud that lidar can scan in one iteration
	private const int maxElements = 10;

	// Amount of rotation. 
	private float bearing = 0f;

	// Used to start or stop the motor
	private int activate;

	// Case
	private int code;

	// List of Waypoints in worldspace
	private Transform[] listOfWayPoints;

	// List of Waypoints being visited
	private List<Vector3> listOfVisitedWayPoints;

	private Stack<Vector3> s = new Stack <Vector3>();

	private int waypointIndex;

	private bool hasTurnBack = false;

	private bool hasCaptured = false;


	// Use this for initialization
	void Start () {		
		vision = GameObject.Find ("Lens 1 (Transmitter)").GetComponent<LidarVision> ();
		lidar = GameObject.Find("Lidar Head").GetComponent<Lidar> ();

		lidarDirections = new float[maxElements];
		pointCloudDistance = new float[maxElements];

		maxDistance = vision.maxDistance;
		count = 0; // current number of point cloud

		// Cases
		code = 0;

		// Start the motor.
		activate = 1;

		// Get all the waypoints found in worldspace.
		listOfWayPoints = GameObject.Find("Waypoints").GetComponentsInChildren<Transform>();
	
		listOfVisitedWayPoints = new List<Vector3> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		// distance from obstacle and robot
		pointCloudDistance [count] = vision.distanceFromObstacle;

		// direction of the lidar
		lidarDirections [count++] = lidar.angle;

		if (count == maxElements) {			
			index = 0;
			for (int i = 1; i < maxElements; i++) {
				// Longest distance in which the laser is not collided with any obstacles.
				if (pointCloudDistance [i] == 0) {
					index = i;
					code = 1;
					break;
				} else if (pointCloudDistance [index] < pointCloudDistance [i]) { // Longest distance in which the laser has collided with certain obstacles.					
					index = i; 
				}		
			}
			if (code == 0) // If not case 1 then case 2
				code = 2;

			count = 0;
		}

		if (code == 1) {
			//Debug.Log ("Turn");
			bearing = 0f;
		} else if (code == 2) {
			// Case 2 and Case 3
			// If the ratio distance is 20 - 100%, then slightly turn
			float ratio = (pointCloudDistance [index] / maxDistance);
			//Debug.Log ("Ratio: " + ratio);
			if (ratio >= 0.2 && ratio <= 1) {
				//Debug.Log ("Slightly Turn");
				bearing = -10f;
			} else {// Turn back
				hasTurnBack = true;
				//Debug.Log ("Turn back");
				bearing = 180f;
			}
		}
		code = 0;		


		////////////////////////////////////////////////////////////////////////////////////////////////
		/*for (int i = 1; i < listOfWayPoints.Length; i++) {
			// Ratio of x and y
			float rZ = transform.position.z;
			float rX = transform.position.x;

			float lZ = listOfWayPoints [i].position.z;
			float lX = listOfWayPoints [i].position.x;

			// Compute the ratio between rx, rz, lz and lx.
			if (rZ > lZ && rX < lX) {
				if ((lZ / rZ) >= 0.8 && (rX / lX) >= 0.8) {					
					if (listOfVisitedWayPoints.Count == 0) {
						listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
						s.Push (listOfWayPoints [i].position);
						transform.LookAt (listOfWayPoints [i].position);
						hasTurnBack = false;
						Debug.Log ("Push");
					}
					else {
						bool found = false;
						for(int j = 0; j < listOfVisitedWayPoints.Count; j++)
						{
							if (listOfVisitedWayPoints [j] == listOfWayPoints [i].position) {
								found = true;
								break;
							}
						}
						if (!found) {
							listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
							s.Push (listOfWayPoints [i].position);
							transform.LookAt (listOfWayPoints [i].position);
							hasTurnBack = false;
							Debug.Log ("Push");
						}
					}
					break;
				}
			} else if (rZ < lZ && rX > lX) {
				if ((rZ / lZ) >= 0.8 && (lX / rX) >= 0.8) {
					if (listOfVisitedWayPoints.Count == 0) {
						listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
						s.Push (listOfWayPoints [i].position);
						transform.LookAt (listOfWayPoints [i].position);
						hasTurnBack = false;
						Debug.Log ("Push");
					}
					else {
						bool found = false;
						for(int j = 0; j < listOfVisitedWayPoints.Count; j++)
						{
							if (listOfVisitedWayPoints [j] == listOfWayPoints [i].position) {
								found = true;
								break;
							}
						}
						if (!found) {
							listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
							s.Push (listOfWayPoints [i].position);
							transform.LookAt (listOfWayPoints [i].position);
							hasTurnBack = false;
							Debug.Log ("Push");
						}
					}
					break;
				}
			} else if (rZ > lZ && rX > lX) {
				if ((lZ / rZ) >= 0.8 && (lX / rX) >= 0.8) {
					if (listOfVisitedWayPoints.Count == 0) {
						listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
						s.Push (listOfWayPoints [i].position);
						transform.LookAt (listOfWayPoints [i].position);
						hasTurnBack = false;
						Debug.Log ("Push");
					}
					else {
						bool found = false;
						for(int j = 0; j < listOfVisitedWayPoints.Count; j++)
						{
							if (listOfVisitedWayPoints [j] == listOfWayPoints [i].position) {
								found = true;
								break;
							}
						}
						if (!found) {
							listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
							s.Push (listOfWayPoints [i].position);
							transform.LookAt (listOfWayPoints [i].position);
							hasTurnBack = false;
							Debug.Log ("Push");
						}
					}
					break;
				}
			} else {
				if ((rZ / lZ) >= 0.8 && (rX / lX) >= 0.8) {
					if (listOfVisitedWayPoints.Count == 0) {
						listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
						s.Push (listOfWayPoints [i].position);
						transform.LookAt (listOfWayPoints [i].position);
						hasTurnBack = false;
						Debug.Log ("Push");
					}
					else {
						bool found = false;
						for(int j = 0; j < listOfVisitedWayPoints.Count; j++)
						{
							if (listOfVisitedWayPoints [j] == listOfWayPoints [i].position) {
								found = true;
								break;
							}
						}
						if (!found) {
							listOfVisitedWayPoints.Add (listOfWayPoints [i].position);
							s.Push (listOfWayPoints [i].position);
							transform.LookAt (listOfWayPoints [i].position);
							hasTurnBack = false;
							Debug.Log ("Push");
						}
					}
					break;
				}
			}
		}*/

		/*if (hasTurnBack && s.Count != 0) {
			if (transform.position != s.Peek ()) {
				transform.LookAt (new Vector3(s.Peek().x, 2.67f, s.Peek().z));
				Debug.Log ("Locate: " + s.Peek ());
			}
			else if(transform.position == s.Peek()) {
				s.Pop ();
				Debug.Log ("Pop");
			}
		}*/


		transform.localRotation = Quaternion.Slerp (transform.localRotation, Quaternion.Euler (0, lidarDirections[index]*activate + bearing, 0), Time.deltaTime * rotationSpeed);
		transform.Translate (0, 0, -Time.deltaTime * speed * activate);
	}

	void OnCollisionEnter(Collision col){
		if (col.collider.tag == "goal") {			
			bearing = 180f;
			activate = 0; // turn off the motor.
			vision.hasDetectGoal = true;
			Debug.Log("Goal");

			if (!hasCaptured) {
				hasCaptured = true;								
				RenderTexture.active = rt;
				Texture2D virtualPhoto = new Texture2D (200, 200, TextureFormat.RGB24, false);
				virtualPhoto.ReadPixels (new Rect (0, 0, 200, 200), 0, 0);
				
				byte[] bytes;
				bytes = virtualPhoto.EncodeToPNG ();
				
				Debug.Log (Application.persistentDataPath);
				System.IO.File.WriteAllBytes ("D:/map.png", bytes);
			}
		}
	}
}
