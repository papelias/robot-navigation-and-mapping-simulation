﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lidar : MonoBehaviour {

	// Speed of rotating the lidar side to side
	private float speed = 50f;
	// amount of rotation
	public float angle;

	// Use this for initialization
	void Start () {		
	}

	void Update() {
		transform.localRotation = Quaternion.AngleAxis ((Mathf.PingPong (Time.time*3, 2)*speed) - 45, Vector3.up);
		angle = transform.eulerAngles.y;
	}
}
